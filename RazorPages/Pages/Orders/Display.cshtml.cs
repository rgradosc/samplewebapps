using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLibrary.Data;
using DataLibrary.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPages.Models;

namespace RazorPages.Pages.Orders
{
    public class DisplayModel : PageModel
    {
        private readonly IFoodData _foodData;
        private readonly IOrderData _orderData;

        [BindProperty(SupportsGet = true)]
        public int Id { get; set; }

        [BindProperty]
        public OrderUpdateModel OrderUpdate { get; set; }

        public List<OrderModel> Order { get; set; }

        public string ItemPurchased { get; set; }

        public DisplayModel(IFoodData foodData, IOrderData orderData)
        {
            _foodData = foodData;
            _orderData = orderData;
        }

        public async Task<IActionResult> OnGet()
        {
            Order = await _orderData.AllOrders(Id);

            if (Order != null && Order.Count > 0)
            {
                var food = await _foodData.AllFoods();
                ItemPurchased = food.Where(f => f.Id == Order.First().FoodId).FirstOrDefault()?.Title;
            }

            return Page();
        }

        public async Task<IActionResult> OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await _orderData.UpdateOrders(OrderUpdate.OrderName, OrderUpdate.Id);

            return RedirectToPage("./Display", new { OrderUpdate.Id });
        }
    }
}
