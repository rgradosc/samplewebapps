using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataLibrary.Data;
using DataLibrary.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace RazorPages.Pages.Orders
{
    public class CreateModel : PageModel
    {
        private readonly IFoodData _foodData;
        private readonly IOrderData _orderData;

        public List<SelectListItem> FoodItems { get; set; }

        [BindProperty]
        public OrderModel Order { get; set; }

        public CreateModel(IFoodData foodData, IOrderData orderData)
        {
            _foodData = foodData;
            _orderData = orderData;
        }

        public async Task OnGet()
        {
            var foods = await _foodData.AllFoods();

            FoodItems = new List<SelectListItem>();

            foods.ForEach(f =>
            {
                FoodItems.Add(new SelectListItem { Value = f.Id.ToString(), Text = f.Title });
            });
        }

        public async Task<IActionResult> OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var food = await _foodData.AllFoods();
            Order.Total = Order.Quantity * food.Where(f => f.Id == Order.FoodId).First().Price;

            int id = await _orderData.CreateOrders(Order);

            return RedirectToPage("./Display", new { Id = id});
        }
    }
}
