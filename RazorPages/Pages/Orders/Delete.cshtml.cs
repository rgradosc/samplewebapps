using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLibrary.Data;
using DataLibrary.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace RazorPages.Pages.Orders
{
    public class DeleteModel : PageModel
    {
        private readonly IOrderData _orderData;

        [BindProperty]
        public int Id { get; set; }

        public List<OrderModel> Order { get; set; }

        public DeleteModel(IOrderData orderData)
        {
            _orderData = orderData;
        }

        public async Task OnGet()
        {
            Order = await _orderData.AllOrders(Id);
        }

        public async Task<IActionResult> OnPost()
        {
            await _orderData.RemoveOrders(Id);
            return RedirectToPage("./Create");
        }

    }
}
