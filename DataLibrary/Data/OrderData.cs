﻿using Dapper;
using DataLibrary.Database;
using DataLibrary.Models;
using System.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataLibrary.Data
{
    public class OrderData : IOrderData
    {
        private readonly IDataAccess _dataAccess;
        private readonly ConnectionStringData _connectionString;

        public OrderData(IDataAccess dataAccess, ConnectionStringData connectionString)
        {
            _dataAccess = dataAccess;
            _connectionString = connectionString;
        }

        public async Task<List<OrderModel>> AllOrders(int id)
        {
            string storedProcedureName = "spOrders_GetById";
            var p = new DynamicParameters();
            p.Add("@Id", id);

            return await _dataAccess.LoadData<OrderModel, dynamic>(
                                                storedProcedureName,
                                                p,
                                                _connectionString.SqlConnectionName);
        }

        public Task<int> UpdateOrders(string name, int id)
        {
            string storedProcedureName = "spOrders_UpdateName";
            var p = new DynamicParameters();
            p.Add("@OrderName", name);
            p.Add("@Id", id);

            return _dataAccess.SaveData<dynamic>(
                                            storedProcedureName,
                                            p,
                                            _connectionString.SqlConnectionName);
        }

        public Task<int> RemoveOrders(int id)
        {
            string storedProcedureName = "spOrders_Delete";
            var p = new DynamicParameters();
            p.Add("@Id", id);

            return _dataAccess.SaveData<dynamic>(
                                            storedProcedureName,
                                            p,
                                            _connectionString.SqlConnectionName);
        }

        public async Task<int> CreateOrders(OrderModel order)
        {
            string storedProcedureName = "spOrders_Insert";
            var p = new DynamicParameters();
            p.Add("@OrderName", order.OrderName);
            p.Add("@OrderDate", order.OrderDate);
            p.Add("@FoodId", order.FoodId);
            p.Add("@Quantity", order.Quantity);
            p.Add("@Total", order.Total);
            p.Add("@Id", DbType.Int32, direction: ParameterDirection.Output);

            await _dataAccess.SaveData<dynamic>(
                                            storedProcedureName,
                                            p,
                                            _connectionString.SqlConnectionName);

            return p.Get<int>("@Id");
        }
    }
}
