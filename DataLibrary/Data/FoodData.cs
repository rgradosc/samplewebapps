﻿using DataLibrary.Database;
using DataLibrary.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataLibrary.Data
{
    public class FoodData : IFoodData
    {
        private readonly IDataAccess _dataAccess;
        private readonly ConnectionStringData _connectionStringData;

        public FoodData(IDataAccess dataAccess, ConnectionStringData connectionString)
        {
            _dataAccess = dataAccess;
            _connectionStringData = connectionString;
        }

        public async Task<List<FoodModel>> AllFoods()
        {
            string storedProcedureName = "spFood_All";

            return await _dataAccess.LoadData<FoodModel, dynamic>(
                                                storedProcedureName,
                                                new { },
                                                _connectionStringData.SqlConnectionName);
        }

    }
}
