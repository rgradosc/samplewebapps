﻿using DataLibrary.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataLibrary.Data
{
    public interface IOrderData
    {
        Task<List<OrderModel>> AllOrders(int id);
        Task<int> CreateOrders(OrderModel order);
        Task<int> RemoveOrders(int id);
        Task<int> UpdateOrders(string name, int id);
    }
}