﻿create procedure [dbo].[spFood_All]
as
begin
	set nocount on;
	
	select [Id], [Title], [Description], [Price] 
	from dbo.Food
end
go