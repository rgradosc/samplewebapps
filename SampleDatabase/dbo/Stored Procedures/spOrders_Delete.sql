﻿create procedure [dbo].[spOrders_Delete]
	@Id int
as
begin
	set nocount on;

	delete
	from dbo.[Order]
	where Id = @Id;
end
go
